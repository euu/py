#!/usr/bin/env python
# add null to the file names like 1-letters.txt
from os import walk, getcwd, rename
from os.path import join
for dirpath, dirs, files in walk(getcwd(),topdown=False):
    for file in files:
        if file[0] in '123456789' and file[1] in '. -':
            print("Renaming file: " + file)
            rename(join(dirpath,file), join(dirpath,'0'+file))
    for dir in dirs:
        if dir[0] in '123456789' and dir[1] in '. -':
            print("Renaming dir: " + dir)
            rename(join(dirpath,dir), join(dirpath,'0'+dir))
input("Press Enter to continue...")
