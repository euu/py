#!/usr/bin/env python
# move files from nested folders 
# one level up and renaming them dir+name

from os import listdir, getcwd, rename
from os.path import isdir, join
import os.path
import shutil

path=getcwd()
for folder in listdir(path):
    if isdir(folder):
        os.chdir(folder) 
        inpath=join(path,folder)
        for f in listdir(inpath):
            os.rename(f, folder+"-"+f)
        for f in listdir(inpath):
            print(f,folder)
            print(join(inpath,f))
            print(join(path,f))
            shutil.move(join(inpath,f), join(path,f))
        os.chdir(path)